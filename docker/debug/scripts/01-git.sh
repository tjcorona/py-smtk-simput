#!/bin/bash

export WORKDIR=/work

# -----------------------------------------------------------------------------
# VTK/release
# -----------------------------------------------------------------------------

mkdir -p "$WORKDIR/VTK/build" && cd "$WORKDIR/VTK" && \
    git clone https://gitlab.kitware.com/VTK/vtk.git src && \
    cd "$WORKDIR/VTK/src" && \
    git checkout release && \
    git submodule update --init --recursive

# -----------------------------------------------------------------------------
# CMB/sha
# -----------------------------------------------------------------------------

export CMB_SHA="34bb8720437446e1b5a4028dfe7b910e21618f8f"

mkdir -p "$WORKDIR/CMB/build" && cd "$WORKDIR/CMB" && \
    git clone https://gitlab.kitware.com/cmb/cmb-superbuild.git src && \
    cd "$WORKDIR/CMB/src" && \
    git checkout $CMB_SHA && \
    git submodule update --init

# -----------------------------------------------------------------------------
# SMTK/master
# -----------------------------------------------------------------------------

mkdir -p "$WORKDIR/smtk/build" && cd "$WORKDIR/smtk" && \
    git clone https://gitlab.kitware.com/cmb/smtk.git src && \
    cd "$WORKDIR/smtk/src" && \
    git submodule update --init
