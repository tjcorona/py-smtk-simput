#!/bin/bash

export WORKDIR=/work

cd "$WORKDIR/CMB/build" && \
    $CMAKE \
      -S ${WORKDIR}/CMB/src \
      -B ${WORKDIR}/CMB/build \
      -G Ninja \
      -D DEVELOPER_MODE_smtk:BOOL=ON \
      -D ENABLE_DOCUMENTATION:BOOL=OFF \
      -D ENABLE_cmb:BOOL=OFF \
      -D ENABLE_cmbusersguide:BOOL=OFF \
      -D ENABLE_qt5:BOOL=OFF \
      -D ENABLE_smtkprojectmanager:BOOL=OFF \
      -D ENABLE_smtkresourcemanagerstate:BOOL=OFF \
      -D ENABLE_smtkusersguide:BOOL=OFF \
      -D USE_SYSTEM_boost:BOOL=ON \
      -D USE_SYSTEM_PNG:BOOL=ON \
      -D USE_SYSTEM_python:BOOL=ON \
      -D ENABLE_python3:BOOL=ON \
      -D BUILD_TESTING:BOOL=OFF \
      -D USE_SYSTEM_zlib:BOOL=ON \
      -D TEST_smtk:BOOL=OFF \
      -D JSON_BuildTests=OFF \
      -D PYTHON_EXECUTABLE=/opt/conda/bin/python \
      -D PYTHON_INCLUDE_DIR=/opt/conda/include/python3.7m \
      -D PYTHON_LIBRARY=/opt/conda/lib/libpython3.7m.so \
      -D __BUILDBOT_INSTALL_LOCATION=/opt/cmb_superbuild

$CMAKE --build "$WORKDIR/CMB/build"
