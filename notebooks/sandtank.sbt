<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <!-- Categories TBD-->
  <!-- Analyses TBD-->
  <!-- Attribute Definitions-->
  <Definitions>
    <!-- The "analysis" attribute is a singleton collecting various doman and solver settings-->
    <AttDef Type="analysis" Label="Analysis">
      <ItemDefinitions>
        <String Name="mode" Label="Mode">
          <DiscreteInfo DefaultIndex="0">
            <Value>Lake</Value>
            <Value>River</Value>
          </DiscreteInfo>
        </String>
        <Double Name="water-level" Label="Water Level" NumberOfRequiredValues="2">
          <ComponentLabels>
            <Label>Left:</Label>
            <Label>Right:</Label>
          </ComponentLabels>
          <DefaultValue>25.0</DefaultValue>
        </Double>
        <Double Name="grid-dimensions" Label="Grid Dimensions" NumberOfRequiredValues="2" AdvanceLevel="1">
          <ComponentLabels>
            <Label>X:</Label>
            <Label>Y:</Label>
          </ComponentLabels>
          <DefaultValue>100.0, 50.0</DefaultValue>
        </Double>
        <Int Name="grid-cells" Label="Grid Cells" NumberOfRequiredValues="2" AdvanceLevel="1">
          <ComponentLabels>
            <Label>X:</Label>
            <Label>Y:</Label>
          </ComponentLabels>
          <DefaultValue>100,50</DefaultValue>
        </Int>
        <Int Name="steps" Label="Computational Steps" AdvanceLevel="1">
          <DefaultValue>100</DefaultValue>
        </Int>
        <Void Name="reset" Label="Reset" Optional="true" IsEnabledByDefault="false"></Void>
      </ItemDefinitions>
    </AttDef>
    <!-- The "region" attribute specifies a label in the input image-->
    <AttDef Type="region" Label="Region" AdvanceLevel="1">
      <ItemDefinitions>
        <Int Name="id" Label="Image Label">
          <BriefDescription>The corresponding label in the image (indicator file?)</BriefDescription>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <!-- The "soil-type" attribute specifies the 4 regions types in our model-->
    <AttDef Type="soil-type" Label="Soil Type" Unique="true">
      <AssociationsDef Name="soil-type-assoc" NumberOfRequiredValues="0" Extensible="true">
        <Accepts>
          <Resource Name="smtk::attribute::Resource" Filter="attribute[type='region']"></Resource>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="identifier" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>An internal persistent identifier (string)</BriefDescription>
        </String>
        <Double Name="permeability" Label="Permeability" Units="darcy" AdvanceLevel="1">
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="specific-storage" Label="Specific Storage" AdvanceLevel="1">
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="porosity" Label="Porosity">
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Group Name="van-genuchten" Label="van Genuchten Parameters" AdvanceLevel="1">
          <ItemDefinitions>
            <Double Name="alpha" Label="Alpha">
              <DefaultValue>0.1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="nu" Label="Nu">
              <DefaultValue>1.1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">1.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <!-- The "well" attribute specifies vertical wells in the sandtank-->
    <AttDef Type="well" Label="Well">
      <ItemDefinitions>
        <Double Name="x" Label="X Position">
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="depth" Label="Depth">
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Group Name="pump" Label="Pump" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <Double Name="rate" Label="Flow Rate"></Double>
            <String Name="mode" Label="Type">
              <DiscreteInfo>
                <Value Enum="Injection">injection</Value>
                <Value Enum="Extraction">extraction</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <!-- The top-level view is a tabbed group.-->
    <View Type="Group" Name="Region 3 Tank" TopLevel="true" TabPosition="North" FilterByCategory="false">
      <Views>
        <View Title="Analysis"/>
        <View Title="Soil Types"/>
        <View Title="Wells"/>
      </Views>
    </View>
    <!-- The "Analysis" view is type "Instanced", which creates a singleton attribute.-->
    <View Type="Instanced" Title="Analysis">
      <InstancedAttributes>
        <Att Type="analysis" Name="instance:analysis"></Att>
      </InstancedAttributes>
    </View>
    <!-- The "Wells" view is type "Attribute", which provides users controls to create/delete attributes.-->
    <View Type="Attribute" Name="Wells">
      <AttributeTypes>
        <Att Type="well"></Att>
      </AttributeTypes>
    </View>
    <!-- The "Soil Types" view is type "Attribute", for specifying different soil types-->
    <View Type="Attribute" Title="Soil Types" RequireAllAssociated="true" AvailableLabel="Unassigned" CurrentLabel="Assigned" DisableTopButtons="false" AssociationTitle="All regions must be assigned a soil type">
      <AttributeTypes>
        <Att Type="soil-type"></Att>
      </AttributeTypes>
    </View>
    <!-- The "Regions" view is used to identify regions in the input (indicator?) image-->
    <View Type="Attribute" Title="Regions" DisableTopButtons="true">
      <AttributeTypes>
        <Att Type="region"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
